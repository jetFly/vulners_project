from django.db import models

class SavedObjects(models.Model):

    object_id = models.CharField("Object_id", max_length=200, blank=False, unique=True)

    def __str__(self):
        return self.object_id
