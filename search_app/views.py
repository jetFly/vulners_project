from django.shortcuts import render
from django.views import View
import requests
import urllib.parse
from .models import SavedObjects


class IndexView(View):

    @staticmethod
    def get(request):
        saved_objects = list(SavedObjects.objects.all().values_list('object_id', flat=True))

        # configuration for lucene query
        vendor_lucene_field = 'title'  # I did't find the field 'vendor' in the documentation. Therefore, I use the field 'title' as the most suitable.
        type_lucene_field = 'bulletinFamily'

        search_vendor = request.GET.get('vendor') or ''
        search_type = request.GET.get('type') or ''

        if search_vendor and search_type:
            query_string = '%s:%s AND %s:%s' % (vendor_lucene_field, search_vendor, type_lucene_field, search_type)
        elif search_vendor:
            query_string = '%s:%s' % (vendor_lucene_field, search_vendor)
        else:
            query_string = '%s:%s' % (type_lucene_field, search_type)

        query_url = urllib.parse.quote_plus(query_string)
        api_search_url = 'https://vulners.com/api/v3/search/lucene/?query={}'.format(query_url)

        elements = []
        r = requests.get(api_search_url)

        if r.status_code == 200:
            json_data = r.json()
            search_result = json_data.get('data').get('search')

            for item in search_result:
                elements.append(item.get('_source'))

        context = {
            'elements': elements,
            'saved': saved_objects,
            'query_type': search_type,
            'query_vendor': search_vendor,
        }

        return render(request, 'search_app/index.html', context=context)

    @staticmethod
    def post(request):
        """
        Saving selected items by ids
        :param request:
        :return: default view
        """
        object_ids = request.POST.getlist('object_id')
        for object_id in object_ids:
            SavedObjects.objects.update_or_create(object_id=object_id)

        return IndexView.get(request)


class SavedView(View):
    @staticmethod
    def get(request):
        elements = []
        saved_objects = SavedObjects.objects.all()

        for object_id in saved_objects:
            api_search_url = 'https://vulners.com/api/v3/search/id/?id={}'.format(object_id)
            r = requests.get(api_search_url)

            if r.status_code == 200:
                json_data = r.json()
                elements.append(json_data.get('data').get('documents').get(str(object_id)))

        context = {
            'elements': elements
        }
        return render(request, 'search_app/index.html', context=context)

    @staticmethod
    def post(request):
        """
        Removing saved items by ids
        :param request:
        :return: saved view
        """
        object_ids = request.POST.getlist('object_id')
        for object_id in object_ids:
            SavedObjects.objects.filter(object_id=object_id).delete()

        return SavedView.get(request)
